/*Student: Rodrigo Rivas
 * ID: 1910674 */
package lab2;

public class BikeStore {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bicycle [] bikes = new Bicycle[4];
		
		bikes[0] = new Bicycle("Garneau",18,23);
		bikes[1] = new Bicycle("Specialized",24,57.9);
		bikes[2] = new Bicycle("Trek",27,72);
		bikes[3] = new Bicycle("Giant",21,40);
		
		
		for(int i=0; i<bikes.length;i++) {
			System.out.println(bikes[i]);
		}
	}

}
