/*Student: Rodrigo Rivas
 * ID: 1910674 */
package lab2;

public class Bicycle {
	private String manufacturer;
	private int numberGears;
	private double maxSpeed;
	
	public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
		this.manufacturer = manufacturer;
		this.numberGears = numberGears;
		this.maxSpeed = maxSpeed;
	}
	
	
	public String getManufacturer() {
		return manufacturer;
	}
	
	public int getNumberGears() {
		return numberGears;
	}
	
	public double getMaxSpeed() {
		return maxSpeed;
	}
	
	public String toString() {
		return "Manufacturer: " + manufacturer + ", Number of Gears: " + numberGears + ", Max Speed:" + maxSpeed;
	}
}
